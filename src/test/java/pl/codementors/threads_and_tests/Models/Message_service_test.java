package pl.codementors.threads_and_tests.Models;

import org.junit.Before;
import org.junit.Test;
import pl.codementors.threads_and_tests.Exceptions.Message_exception;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Java6Assertions.catchThrowable;
import static org.junit.Assert.assertNotNull;

public class Message_service_test {
    private Message message1, message2, message3, message4, not_initialized_message;
    private Message_catalogue message_catalogue;
    private Message_repository message_repository;
    private Message_service message_service;

    @Before
    public void set_up() {
        message1 = new Message("Title1", "Author1", "Content1");
        message2 = new Message("Title2", "Author2", "Content2");
        message3 = new Message("Title3", "Author1", "Content3");
        message4 = new Message("Title4", "Author4", "Content4");
        message_catalogue = new Message_catalogue();
        message_repository = new Message_repository(message_catalogue);
        message_service = new Message_service(message_repository);
    }

    @Test
    public void message_service_created_and_empty() {
        assertNotNull(message_service);
        assertThat(
                message_service
                        .get_all_messages()
                        .size())
                .isEqualTo(0);
    }

    @Test
    public void message_added_when_exists_but_not_in_catalogue() throws Message_exception {
        message_service.add_if_no_exists(message1);
        message_service.add_if_no_exists(message2);

        assertThat(message_service.get_all_messages().size()).isEqualTo(2);
    }

    @Test
    public void message_not_added_when_not_initilialized() throws Message_exception {
        message_service.add_if_no_exists(message1);

        Throwable throwable = catchThrowable(() -> message_service.add_if_no_exists(not_initialized_message));
        assertThat(throwable).isInstanceOf(Message_exception.class);

        assertThat(
                message_service
                        .get_all_messages()
                        .size())
                .isEqualTo(1);
        assertThat(
                message_service.get_all_messages())
                .doesNotContain(not_initialized_message);
    }

    @Test
    public void message_not_added_when_already_in_catalogue() throws Message_exception {
        message_service.add_if_no_exists(message1);

        Throwable throwable = catchThrowable(() -> message_service.add_if_no_exists(message1));
        assertThat(throwable).isInstanceOf(Message_exception.class);

        assertThat(
                message_service
                        .get_all_messages()
                        .size())
                .isEqualTo(1);
    }

    @Test
    public void catalogue_state_do_not_change_when_null_passed_to_add() throws Message_exception {
        message_service.add_if_no_exists(message1);
        message_service.add_if_no_exists(message2);

        Throwable throwable = catchThrowable(() -> message_service.add_if_no_exists(null));
        assertThat(throwable).isInstanceOf(Message_exception.class);

        assertThat(
                message_service
                        .get_all_messages()
                        .size())
                .isEqualTo(2);
    }

    @Test
    public void message_deleted_if_in_catalogue() throws Message_exception {
        message_service.add_if_no_exists(message1);
        message_service.add_if_no_exists(message2);
        message_service.add_if_no_exists(message3);

        message_service.delete_if_exists(message1);

        assertThat(message_service.get_all_messages().size()).isEqualTo(2);
        assertThat(message_service.get_all_messages()).doesNotContain(message1);
    }

    @Test
    public void message_not_deleted_if_not_in_catalogue() throws Message_exception {
        message_service.add_if_no_exists(message1);
        message_service.add_if_no_exists(message2);

        Throwable throwable = catchThrowable(() -> message_service.delete_if_exists(message3));
        assertThat(throwable).isInstanceOf(Message_exception.class);

        assertThat(
                message_service
                        .get_all_messages()
                        .size())
                .isEqualTo(2);
    }

    @Test
    public void catalogue_state_not_changed_when_null_passed_to_delete() throws Message_exception {
        message_service.add_if_no_exists(message1);
        message_service.add_if_no_exists(message2);

        Throwable throwable = catchThrowable(() -> message_service.delete_if_exists(null));
        assertThat(throwable).isInstanceOf(Message_exception.class);

        assertThat(
                message_service
                        .get_all_messages()
                        .size())
                .isEqualTo(2);
    }

    @Test
    public void get_all_messages_return_all_messages() throws Message_exception {
        message_service.add_if_no_exists(message1);
        message_service.add_if_no_exists(message2);

        assertThat(message_service.get_all_messages().size()).isEqualTo(2);
        assertThat(message_service.get_all_messages()).contains(message1);
    }

    @Test
    public void get_all_messages_returns_unmodificable_list() throws Message_exception {
        message_service.add_if_no_exists(message1);
        message_service.add_if_no_exists(message2);

        Throwable throwable = catchThrowable(() -> message_service.get_all_messages().remove(0));
        assertThat(throwable).isInstanceOf(UnsupportedOperationException.class);

        assertThat(
                message_service
                        .get_all_messages()
                        .size())
                .isEqualTo(2);
    }

    @Test
    public void messages_are_filtered_by_title() throws Message_exception {
        message_service.add_if_no_exists(message1);
        message_service.add_if_no_exists(message2);
        message_service.add_if_no_exists(message3);

        assertThat(
                message_service
                        .filter_by_title("Title1")
                        .get(0)
                        .getTitle())
                .isEqualTo(message1.getTitle());
    }

    @Test
    public void filter_by_title_returns_unmodificable_list() throws Message_exception {
        message_service.add_if_no_exists(message1);
        message_service.add_if_no_exists(message2);

        Throwable throwable = catchThrowable(() ->
                message_service
                        .filter_by_title("Tytuł1")
                        .remove(0));
        assertThat(throwable).isInstanceOf(UnsupportedOperationException.class);

        assertThat(
                message_service
                        .get_all_messages()
                        .size())
                .isEqualTo(2);
    }

    @Test
    public void message_service_do_not_change_state_when_null_passed_to_filter_by_title() throws Message_exception {
        message_service.add_if_no_exists(message1);
        message_service.add_if_no_exists(message2);

        Throwable throwable = catchThrowable(() -> message_service.filter_by_title(null));
        assertThat(throwable).isInstanceOf(Message_exception.class);

        assertThat(
                message_service
                        .get_all_messages()
                        .size())
                .isEqualTo(2);
    }

    @Test
    public void messages_are_filtered_by_author() throws Message_exception {
        message_service.add_if_no_exists(message1);
        message_service.add_if_no_exists(message2);
        message_service.add_if_no_exists(message3);

        assertThat(
                message_service
                .filter_by_author("Author1")
                .get(0)
                .getAuthor())
                .isEqualTo(message1.getAuthor());
    }

    @Test
    public void filter_by_author_returns_unmodificable_list() throws Message_exception {
        message_service.add_if_no_exists(message1);
        message_service.add_if_no_exists(message2);

        Throwable throwable = catchThrowable(() ->
                message_service
                        .filter_by_author("Author1")
                        .remove(0));
        assertThat(throwable).isInstanceOf(UnsupportedOperationException.class);

        assertThat(
                message_service
                        .get_all_messages()
                        .size())
                .isEqualTo(2);
    }

    @Test
    public void message_service_do_not_change_state_when_null_passed_to_filter_by_author() throws Message_exception {
        message_service.add_if_no_exists(message1);
        message_service.add_if_no_exists(message2);

        Throwable throwable = catchThrowable(() -> message_service.filter_by_author(null));
        assertThat(throwable).isInstanceOf(Message_exception.class);

        assertThat(
                message_service
                        .get_all_messages()
                        .size())
                .isEqualTo(2);
    }

    @Test
    public void messages_are_filtered_by_content() throws Message_exception {
        message_service.add_if_no_exists(message1);
        message_service.add_if_no_exists(message2);
        message_service.add_if_no_exists(message3);

        assertThat(
                message_service
                        .filter_by_content("Content1")
                        .get(0)
                        .getContent())
                .isEqualTo(message1.getContent());
    }

    @Test
    public void filter_by_content_returns_unmodificable_list() throws Message_exception {
        message_service.add_if_no_exists(message1);
        message_service.add_if_no_exists(message2);

        Throwable throwable = catchThrowable(() ->
                message_service
                        .filter_by_content("Content1")
                        .remove(0));
        assertThat(throwable).isInstanceOf(UnsupportedOperationException.class);

        assertThat(
                message_service
                        .get_all_messages()
                        .size())
                .isEqualTo(2);
    }

    @Test
    public void message_service_do_not_change_state_when_null_passed_to_filter_by_content() throws Message_exception {
        message_service.add_if_no_exists(message1);
        message_service.add_if_no_exists(message2);

        Throwable throwable = catchThrowable(() -> message_service.filter_by_content(null));
        assertThat(throwable).isInstanceOf(Message_exception.class);

        assertThat(
                message_service
                        .get_all_messages()
                        .size())
                .isEqualTo(2);
    }

    @Test
    public void messages_are_filtered_by_date() throws Message_exception {
        message_service.add_if_no_exists(message1);
        message_service.add_if_no_exists(message2);

        assertThat(
                message_service
                        .filter_by_date(LocalDateTime.now().getDayOfMonth())
                        .get(0)
                        .getDateOfCreation())
                .isEqualTo(message1.getDateOfCreation());
    }

    @Test
    public void filter_by_date_returns_unmodificable_list() throws Message_exception {
        message_service.add_if_no_exists(message1);
        message_service.add_if_no_exists(message2);

        Throwable throwable = catchThrowable(() ->
                message_service
                        .filter_by_date(LocalDateTime.now().getDayOfMonth())
                        .remove(0));
        assertThat(throwable).isInstanceOf(UnsupportedOperationException.class);

        assertThat(
                message_service
                        .get_all_messages()
                        .size())
                .isEqualTo(2);
    }

    @Test
    public void message_service_do_not_change_state_when_zero_passed_to_filter_by_date() throws Message_exception {
        message_service.add_if_no_exists(message1);
        message_service.add_if_no_exists(message2);

        Throwable throwable = catchThrowable(() -> message_service.filter_by_date(0));
        assertThat(throwable).isInstanceOf(Message_exception.class);

        assertThat(
                message_service
                        .get_all_messages()
                        .size())
                .isEqualTo(2);
    }

    @Test
    public void message_service_do_not_change_state_when_date_abowe_31_passed_as_param() throws Message_exception {
        message_service.add_if_no_exists(message1);
        message_service.add_if_no_exists(message2);

        Throwable throwable = catchThrowable(() -> message_service.filter_by_date(33));
        assertThat(throwable).isInstanceOf(Message_exception.class);

        assertThat(
                message_service
                        .get_all_messages()
                        .size())
                .isEqualTo(2);
    }

    @Test
    public void multifiltered_returns_messages_by_title() throws Message_exception {
        message_service.add_if_no_exists(message1);
        message_service.add_if_no_exists(message2);
        message_service.add_if_no_exists(message3);

        assertThat(
                message_service
                        .multifiltered("Title1", null, null, 0)
                        .size())
                .isEqualTo(1);
        assertThat(
                message_service
                        .multifiltered("Title1", null, null, 0)
                        .get(0))
                .isEqualByComparingTo(message1);
    }

    @Test
    public void multifiltered_returns_messages_by_two_title_and_author() throws Message_exception {
        message_service.add_if_no_exists(message1);
        message_service.add_if_no_exists(message2);
        message_service.add_if_no_exists(message3);

        assertThat(
                message_service
                        .multifiltered("Title1", "Author2", null, 0)
                        .size())
                .isEqualTo(2);
        assertThat(
                message_service
                        .multifiltered("Title1", "Author2", null, 0)
                        .get(0))
                .isEqualByComparingTo(message1);
        assertThat(
                message_service
                        .multifiltered("Title1", "Author2", null, 0)
                        .get(1))
                .isEqualByComparingTo(message2);
    }

    @Test
    public void multifiltered_returns_messages_by_title_author_and_content() throws Message_exception {
        message_service.add_if_no_exists(message1);
        message_service.add_if_no_exists(message2);
        message_service.add_if_no_exists(message3);

        assertThat(
                message_service
                        .multifiltered("Title1", "Author2", "Content3", 0)
                        .size())
                .isEqualTo(3);
        assertThat(
                message_service
                        .multifiltered("Title1", "Author2", "Content3", 0)
                        .get(0))
                .isEqualByComparingTo(message1);
        assertThat(
                message_service
                        .multifiltered("Title1", "Author2", "Content3", 0)
                        .get(1))
                .isEqualByComparingTo(message2);
        assertThat(
                message_service
                        .multifiltered("Title1", "Author2", "Content3", 0)
                        .get(2))
                .isEqualByComparingTo(message3);
    }

    @Test
    public void multifiltered_returns_messages_by_title_author_content_and_date() throws Message_exception {
        message_service.add_if_no_exists(message1);
        message_service.add_if_no_exists(message2);
        message_service.add_if_no_exists(message3);
        message_service.add_if_no_exists(message4);

        int day = LocalDateTime.now().getDayOfMonth();

        assertThat(
                message_service
                        .multifiltered("Title1", "Author2", "Content3", day)
                        .size())
                .isEqualTo(7);
        assertThat(
                message_service
                        .multifiltered("Title1", "Author2", "Content3", day)
                        .get(0))
                .isEqualByComparingTo(message1);
        assertThat(
                message_service
                        .multifiltered("Title1", "Author2", "Content3", day)
                        .get(1))
                .isEqualByComparingTo(message2);
        assertThat(
                message_service
                        .multifiltered("Title1", "Author2", "Content3", day)
                        .get(2))
                .isEqualByComparingTo(message3);
        assertThat(
                message_service
                        .multifiltered("Title1", "Author2", "Content3", day)
                        .get(3).getDateOfCreation().getDayOfMonth()).isEqualTo(day);
    }

    @Test
    public void service_do_not_change_state_when_all_params_in_multifiltered_are_null_and_date_0() throws Message_exception {
        message_service.add_if_no_exists(message1);
        message_service.add_if_no_exists(message2);
        message_service.add_if_no_exists(message3);

        assertThat(
                message_service
                        .get_all_messages()
                        .size())
                .isEqualTo(3);
        assertThat(
                message_service
                        .multifiltered(null, null, null, 0)
                        .size())
                .isEqualTo(0);
    }
}