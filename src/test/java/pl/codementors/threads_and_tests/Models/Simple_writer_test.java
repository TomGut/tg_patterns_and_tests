package pl.codementors.threads_and_tests.Models;

import org.junit.Before;
import org.junit.Test;
import pl.codementors.threads_and_tests.Exceptions.Message_not_exist_exception;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Java6Assertions.catchThrowable;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.doCallRealMethod;
import static org.powermock.api.mockito.PowerMockito.mock;

public class Simple_writer_test {
    private Message message1, message2;
    private List<Message> messages;
    private Simple_writer simple_writer;
    private Simple_writer mocked_writer;

    @Before
    public void set_up(){
        message1 = new Message("Title1", "Author1", "Content1");
        message2 = new Message("Title2", "Author2", "Content2");
        messages = new ArrayList<>();
        simple_writer = new Simple_writer();
        mocked_writer = mock(Simple_writer.class);
    }

    @Test
    public void simple_writer_created_empty_and_not_null(){
        assertNotNull(simple_writer);
    }

    @Test
    public void list_of_messages_written_when_not_empty() throws Message_not_exist_exception {
        messages.add(message1);
        messages.add(message2);

        mocked_writer.write(messages);

        verify(mocked_writer).write(messages);
    }

    @Test
    public void list_of_messages_not_written_when_empty() {
        Throwable throwable = catchThrowable(() -> simple_writer.write(messages));
        assertThat(throwable).isInstanceOf(Message_not_exist_exception.class);

        assertThat(messages.size()).isEqualTo(0);
    }

    //TODO have no idea how to get into void method write() and get needed data to do equals with message1
    @Test
    public void simple_info_of_message_written() throws Message_not_exist_exception {
        messages.add(message1);

        doCallRealMethod()
                .when(mocked_writer)
                .write(messages);

        mocked_writer.write(messages);
    }
}
