package pl.codementors.threads_and_tests.Models;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import pl.codementors.threads_and_tests.Exceptions.Message_exception;
import pl.codementors.threads_and_tests.Exceptions.Message_not_exist_exception;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class Message_catalogue_test {

    private Message message1, message2, message3, message_not_initialized;
    private Message_catalogue message_catalogue;

    @Before
    public void set_up(){
        message1 = new Message("Title1", "Author1", "Content1");
        message2 = new Message("Title2", "Author2", "Content2");
        message3 = new Message("Title3", "Author1", "Content3");
        message_catalogue = new Message_catalogue();
    }

    @Test
    public void message_catalogue_created_and_empty_and_not_null(){
        assertNotNull(message_catalogue);
        assertThat(
                message_catalogue
                        .get_messages()
                        .isEmpty(), is(true));
    }

    @Test
    public void message_added_when_exist() throws Message_exception {
        message_catalogue.add(message1);
        message_catalogue.add(message2);
        message_catalogue.add(message3);

        assertThat(
                message_catalogue
                        .get_messages()
                        .size(), is(3));
        assertThat(
                message_catalogue
                        .get_messages()
                        .contains(message2), is(true));
        assertThat(
                message_catalogue
                        .get_messages()
                        .get(0)
                        .getTitle()
                        .equalsIgnoreCase("Title1"), is(true));
    }

    @Test
    public void message_not_added_when_do_not_exists() throws Message_exception {
        message_catalogue.add(message1);
        message_catalogue.add(message2);

        Throwable throwable = Assertions.catchThrowable(() -> message_catalogue.add(message_not_initialized));
        Assertions
                .assertThat(throwable)
                .isInstanceOf(Message_exception.class);

        assertThat(
                message_catalogue
                        .get_messages()
                        .size(), is(2));
    }

    @Test
    public void null_not_added_when_passed_as_param() throws Message_exception {
        message_catalogue.add(message1);

        Throwable throwable = Assertions.catchThrowable(() -> message_catalogue.add(null));
        Assertions
                .assertThat(throwable)
                .isInstanceOf(Message_exception.class);

        assertThat(
                message_catalogue
                        .get_messages()
                        .size(), is(1));
        assertThat(
                message_catalogue
                        .get_messages()
                        .contains(null), is(false));
    }

    @Test
    public void message_deleted_when_in_catalogue() throws Message_exception {
        message_catalogue.add(message1);
        message_catalogue.add(message2);
        message_catalogue.add(message3);

        message_catalogue.delete(message1);

        assertThat(
                message_catalogue
                        .get_messages()
                        .size(), is(2));
        assertThat(
                message_catalogue
                        .get_messages()
                        .contains(message1), is(false));
    }

    @Test
    public void message_not_deleted_when_not_in_catalogue() throws Message_exception {
        message_catalogue.add(message1);
        message_catalogue.add(message2);

        Throwable throwable = Assertions.catchThrowable(() -> message_catalogue.delete(message3));
        Assertions
                .assertThat(throwable)
                .isInstanceOf(Message_not_exist_exception.class);

        assertThat(
                message_catalogue
                        .get_messages()
                        .size(), is(2));
    }

    @Test
    public void catalogue_state_not_changed_when_null_passed_to_delete() throws Message_exception {
        message_catalogue.add(message1);

        Throwable throwable = Assertions.catchThrowable(() -> message_catalogue.delete(null));
        Assertions
                .assertThat(throwable)
                .isInstanceOf(Message_not_exist_exception.class);

        assertThat(
                message_catalogue
                        .get_messages()
                        .size(), is(1));
    }

    @Test
    public void catalogue_returns_not_modifilable_list() throws Message_exception {
        message_catalogue.add(message1);
        message_catalogue.add(message2);

        Throwable throwable = Assertions.catchThrowable(() ->
                message_catalogue
                .get_messages()
                .remove(0));
        Assertions.assertThat(throwable).isInstanceOf(UnsupportedOperationException.class);

        assertThat(
                message_catalogue
                        .get_messages()
                        .contains(message1), is(true));
        assertThat(
                message_catalogue
                        .get_messages()
                        .size(), is(2));
    }
}