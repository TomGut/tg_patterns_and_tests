package pl.codementors.threads_and_tests.Models;

import org.junit.Before;
import org.junit.Test;
import pl.codementors.threads_and_tests.Exceptions.Message_exception;

import static org.assertj.core.api.Assertions.assertThat;

import static org.assertj.core.api.Java6Assertions.catchThrowable;
import static org.junit.Assert.assertNotNull;

public class Message_repository_test {

    private Message message1, message2, message3, message_not_initialized;
    private Message_catalogue message_catalogue;
    private Message_repository message_repository;

    @Before
    public void set_up(){
        message1 = new Message("Title1", "Author1", "Content1");
        message2 = new Message("Title2", "Author2", "Content2");
        message3 = new Message("Title3", "Author1", "Content3");
        message_catalogue = new Message_catalogue();
        message_repository = new Message_repository(message_catalogue);
    }

    @Test
    public void repository_created_empty_and_not_null(){
        assertNotNull(message_repository);
        assertThat(
                message_repository
                        .get_all_messages()
                        .isEmpty())
                .isTrue();
    }

    @Test
    public void repository_get_message_catalogue() {
        assertThat(
                message_repository
                        .getMessage_catalogue())
                .isNotNull();
    }

    @Test
    public void add_message_when_exists() throws Message_exception {
        message_repository.add_message(message1);
        message_repository.add_message(message2);

        assertThat(
                message_repository
                        .get_all_messages()
                        .size())
                .isEqualTo(2);
        assertThat(
                message_repository
                        .get_all_messages()
                        .contains(message1))
                .isTrue();
    }

    @Test
    public void message_not_added_when_dont_exists() throws Message_exception {
        message_repository.add_message(message1);
        message_repository.add_message(message2);

        Throwable throwable = catchThrowable(() -> message_repository.add_message(message_not_initialized));
        assertThat(throwable).isInstanceOf(Message_exception.class);

        assertThat(
                message_repository
                        .get_all_messages()
                        .size())
                .isEqualTo(2);
        assertThat(
                message_repository
                        .get_all_messages()
                        .contains(message_not_initialized))
                .isFalse();
    }

    @Test
    public void null_not_added_when_passed_as_param() throws Message_exception {
        message_repository.add_message(message1);
        message_repository.add_message(message2);

        Throwable throwable = catchThrowable(() -> message_repository.add_message(null));
        assertThat(throwable).isInstanceOf(Message_exception.class);

        assertThat(
                message_repository
                        .get_all_messages()
                        .size())
                .isEqualTo(2);
        assertThat(
                message_repository
                        .get_all_messages()
                        .contains(null))
                .isFalse();
    }

    @Test
    public void message_deleted_when_in_catalogue() throws Message_exception {
        message_repository.add_message(message1);
        message_repository.add_message(message2);

        message_repository.delete_message(message1);

        assertThat(
                message_repository
                        .get_all_messages()
                        .size())
                .isEqualTo(1);
        assertThat(
                message_repository
                        .get_all_messages()
                        .contains(message2))
                .isTrue();
    }

    @Test
    public void message_not_deleted_when_not_in_catalogue() throws Message_exception {
        message_repository.add_message(message1);
        message_repository.add_message(message2);

        Throwable throwable = catchThrowable(() -> message_repository.delete_message(message3));
        assertThat(throwable).isInstanceOf(Message_exception.class);

        assertThat(
                message_repository
                        .get_all_messages()
                        .size())
                .isEqualTo(2);
    }

    @Test
    public void repository_state_not_changed_when_null_passed_to_delete() throws Message_exception {
        message_repository.add_message(message1);
        message_repository.add_message(message3);

        Throwable throwable = catchThrowable(() -> message_repository.delete_message(null));
        assertThat(throwable).isInstanceOf(Message_exception.class);

        assertThat(
                message_repository
                        .get_all_messages()
                        .size())
                .isEqualTo(2);
        assertThat(
                message_repository
                        .get_all_messages()
                        .contains(null))
                .isFalse();
    }

    @Test
    public void repository_returns_all_messages_list() throws Message_exception {
        message_repository.add_message(message1);
        message_repository.add_message(message2);

        assertThat(
                message_repository
                        .get_all_messages()
                        .size())
                .isEqualTo(2);
    }

    @Test
    public void repository_returns_unmodifilable_list() throws Message_exception {
        message_repository.add_message(message1);
        message_repository.add_message(message2);

        Throwable throwable = catchThrowable(() -> message_repository.get_all_messages().remove(0));
        assertThat(throwable).isInstanceOf(UnsupportedOperationException.class);

        assertThat(
                message_repository
                        .get_all_messages()
                        .size())
                .isEqualTo(2);
    }
}