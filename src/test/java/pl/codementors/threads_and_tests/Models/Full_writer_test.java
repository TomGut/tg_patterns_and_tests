package pl.codementors.threads_and_tests.Models;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import pl.codementors.threads_and_tests.Exceptions.Message_not_exist_exception;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Java6Assertions.catchThrowable;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Full_writer.class)
public class Full_writer_test {
    private Message message1, message2;
    private List<Message> messages;
    private Full_writer full_writer;
    private Full_writer mocked_writer;

    @Before
    public void set_up(){
        message1 = new Message("Title1", "Author1", "Content1");
        message2 = new Message("Title2", "Author2", "Content2");
        messages = new ArrayList<>();
        full_writer = new Full_writer();
        mocked_writer = mock(Full_writer.class);
    }

    @Test
    public void full_writer_created_empty_and_not_null(){
        assertNotNull(full_writer);
    }

    @Test
    public void list_of_messages_written_when_not_empty() throws Message_not_exist_exception {
       messages.add(message1);
       messages.add(message2);

       mocked_writer.write(messages);

       verify(mocked_writer).write(messages);
    }

    @Test
    public void list_of_messages_not_written_when_empty() {
        Throwable throwable = catchThrowable(() -> full_writer.write(messages));
        assertThat(throwable).isInstanceOf(Message_not_exist_exception.class);

        assertThat(
                messages
                        .size())
                .isEqualTo(0);
    }

    //TODO have no idea how to get into void method write() and get needed data to do equals with message1
    @Test
    public void full_info_of_message_written() throws Message_not_exist_exception {
        messages.add(message1);

        doCallRealMethod()
                .when(mocked_writer)
                .write(messages);

        mocked_writer.write(messages);
    }
}
