package pl.codementors.threads_and_tests.Models;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import pl.codementors.threads_and_tests.Exceptions.Message_exception;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.StringBufferInputStream;
import java.util.List;
import java.util.Scanner;

import static org.assertj.core.api.Assertions.assertThat;

public class User_search_test {
    private Message message1, message2;
    private Message_catalogue message_catalogue;
    private Message_repository message_repository;
    private Message_service message_service;
    private Simple_writer simple_writer;
    private Full_writer full_writer;
    private User_search user_search;

    @Before
    public void set_up(){
        message1 = new Message("Title1", "Author1", "Content1");
        message2 = new Message("Title2", "Author2", "Content2");
        message_catalogue = new Message_catalogue();
        message_repository = new Message_repository(message_catalogue);
        message_service = new Message_service(message_repository);
        simple_writer = new Simple_writer();
        full_writer = new Full_writer();
        user_search = new User_search();
    }

    @Test
    public void message_printed_when_title_selected() throws Message_exception {
        message_service.add_if_no_exists(message1);

        String user_input = "1";
        InputStream in = new ByteArrayInputStream(user_input.getBytes());
        System.setIn(in);
        Scanner scanner = new Scanner(System.in);

        user_search.search_message(message_service, simple_writer, full_writer, scanner);
    }

    //TODO not know how to get inside search_message() to mock user input in scanner
}
