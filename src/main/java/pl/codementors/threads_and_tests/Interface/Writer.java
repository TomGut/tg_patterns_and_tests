package pl.codementors.threads_and_tests.Interface;

import pl.codementors.threads_and_tests.Exceptions.Message_not_exist_exception;
import pl.codementors.threads_and_tests.Models.Message;

import java.util.List;

public interface Writer {
    void write(List<Message> toWrite) throws Message_not_exist_exception;
}
