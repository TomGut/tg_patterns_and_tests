package pl.codementors.threads_and_tests.Interface;

import pl.codementors.threads_and_tests.Exceptions.Message_exception;
import pl.codementors.threads_and_tests.Models.Message;

import java.util.List;

public interface Repository {
    void add_message(Message message) throws Message_exception;
    void delete_message(Message message) throws Message_exception;
    List<Message> get_all_messages();
}
