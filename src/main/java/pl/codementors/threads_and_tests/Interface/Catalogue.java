package pl.codementors.threads_and_tests.Interface;

import pl.codementors.threads_and_tests.Models.Message;
import pl.codementors.threads_and_tests.Exceptions.Message_exception;

import java.util.List;

public interface Catalogue {
    void add(Message message) throws Message_exception;
    void delete(Message message) throws Message_exception;
    List<Message> get_messages();
}
