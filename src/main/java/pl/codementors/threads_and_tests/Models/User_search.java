package pl.codementors.threads_and_tests.Models;

import pl.codementors.threads_and_tests.Exceptions.Message_exception;
import pl.codementors.threads_and_tests.Exceptions.Message_not_exist_exception;

import java.util.List;
import java.util.Scanner;

public class User_search {

    private String title, author, content;
    private int day;

    public List<Message> search_message(Message_service message_service, Simple_writer simple_writer, Full_writer full_writer, Scanner scanner) throws Message_exception {
        System.out.println("WYSZUKIWANIE WIADOMOŚCI");
        System.out.println();
        System.out.println("Wyszukaj wiadomość po:"
                + "\n1 - po tytule"
                + "\n2 - po autorze"
                + "\n3 - po fragmencie treści"
                + "\n4 - po dacie utworzenia"
                + "\n5 - wybierz połączone kryteria");

        String user_search_input = scanner.next();

        if (user_search_input.equals("1")) {
            System.out.println("Podaj tytuł");
            title = scanner.next();
            System.out.println(message_service.filter_by_title(title));
        } else if (user_search_input.equals("2")) {
            System.out.println("Podaj autora");
            author = scanner.next();
            System.out.println(message_service.filter_by_author(author));
        } else if (user_search_input.equals("3")) {
            System.out.println("Podaj treść");
            content = scanner.next();
            System.out.println(message_service.filter_by_content(content));
        } else if (user_search_input.equals("4")) {
            System.out.println("Podaj dzień utworzenia");
            day = scanner.nextInt();
            System.out.println(message_service.filter_by_date(day));
        } else if (user_search_input.equals("5")) {
            System.out.println("Czy chcesz podać tytuł ? T/N");
            title = scanner.next();
            if (title.equalsIgnoreCase("T")) {
                System.out.println("Podaj tytuł");
                title = scanner.next();
            }
            System.out.println("Czy chcesz podać autora ? T/N");
            author = scanner.next();
            if (author.equalsIgnoreCase("T")) {
                System.out.println("Podaj autora");
                author = scanner.next();
            }
            System.out.println("Czy chcesz podać treść ? T/N");
            content = scanner.next();
            if (content.equalsIgnoreCase("T")) {
                System.out.println("Podaj treść");
                content = scanner.next();
            }
            System.out.println("Czy chcesz podać dzień utworzenia ? T/N");
            String day_of_creation = scanner.next();
            if (day_of_creation.equalsIgnoreCase("T")) {
                System.out.println("Podaj dzień zapisu");
                day = scanner.nextInt();
            }

            System.out.println("WYŚWIETLANIE WYBRANEJ WIADOMOŚCI");
            System.out.println();
            System.out.println("1 - wyświetlanie uproszczone\n2 - wyświetlanie pełne ");

            String user_input = scanner.next();

            if (user_input.equals("1")) {
                simple_writer
                        .write(message_service
                                .multifiltered(title, author, content, day));
            } else if (user_input.equals("2")) {
                full_writer
                        .write(message_service
                                .multifiltered(title, author, content, day));
            } else {
                System.out.println("Podano złą wartość");
            }
        }
        return message_service
                .multifiltered(title, author, content, day);
    }
}

