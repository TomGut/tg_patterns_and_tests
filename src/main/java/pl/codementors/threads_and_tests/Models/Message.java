package pl.codementors.threads_and_tests.Models;

import java.time.LocalDateTime;

public class Message implements Comparable<Message>{
    private String title, author, content;
    private LocalDateTime dateOfCreation;

    public Message(){
    }

    public Message(String title, String author, String content) {
        this.title = title;
        this.author = author;
        this.content = content;
        this.dateOfCreation = LocalDateTime.now();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDateTime getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation() {
        this.dateOfCreation = LocalDateTime.now();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Message message = (Message) o;

        if (title != null ? !title.equals(message.title) : message.title != null) return false;
        if (author != null ? !author.equals(message.author) : message.author != null) return false;
        if (content != null ? !content.equals(message.content) : message.content != null) return false;
        return dateOfCreation != null ? dateOfCreation.equals(message.dateOfCreation) : message.dateOfCreation == null;
    }

    @Override
    public int hashCode() {
        int result = title != null ? title.hashCode() : 0;
        result = 31 * result + (author != null ? author.hashCode() : 0);
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (dateOfCreation != null ? dateOfCreation.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Title: "
                + title
                + "\nauthor: "
                + author
                + "\ncontent: "
                + content
                + "\ndate Of Creation: "
                + dateOfCreation;
    }

    @Override
    public int compareTo(Message o) {
        int ret = title.compareTo(o.title);
        if(ret == 0){
            ret = author.compareTo(o.author);
        }
        if(ret == 0){
            ret = content.compareTo(o.content);
        }
        return ret;
    }
}
