package pl.codementors.threads_and_tests.Models;

import pl.codementors.threads_and_tests.Exceptions.Message_exception;
import pl.codementors.threads_and_tests.Interface.Repository;

import java.util.List;

public class Message_repository implements Repository {
    private Message_catalogue message_catalogue;

    public Message_repository(Message_catalogue message_catalogue) {
        this.message_catalogue = message_catalogue;
    }

    public Message_catalogue getMessage_catalogue() {
        return message_catalogue;
    }

    public void add_message(Message message) throws Message_exception {
        message_catalogue.add(message);
    }

    public void delete_message(Message message) throws Message_exception {
        message_catalogue.delete(message);
    }

    public List<Message> get_all_messages(){
        return message_catalogue.get_messages();
    }
}
