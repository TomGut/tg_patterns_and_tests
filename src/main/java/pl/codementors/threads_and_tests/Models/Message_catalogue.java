package pl.codementors.threads_and_tests.Models;

import pl.codementors.threads_and_tests.Exceptions.Message_exception;
import pl.codementors.threads_and_tests.Exceptions.Message_not_exist_exception;
import pl.codementors.threads_and_tests.Interface.Catalogue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class Message_catalogue implements Catalogue {
    private List<Message> messages = new ArrayList<>();

    public Message_catalogue(){
    }

    public void add(Message message) throws Message_exception {
        if(message != null){
            messages.add(message);
        }else{
            throw new Message_exception();
        }
    }

    public void delete(Message message) throws Message_not_exist_exception {
        if(!messages.contains(message) || message == null){
            throw new Message_not_exist_exception("wiadomości nie ma w katalogu");
        }else{
            messages.remove(message);
        }
    }

    public List<Message> get_messages(){
        return Collections.unmodifiableList(messages);
    }
}
