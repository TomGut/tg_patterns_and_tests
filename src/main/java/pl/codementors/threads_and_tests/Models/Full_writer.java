package pl.codementors.threads_and_tests.Models;

import pl.codementors.threads_and_tests.Exceptions.Message_not_exist_exception;
import pl.codementors.threads_and_tests.Interface.Writer;

import java.util.List;

public class Full_writer implements Writer {
    public Full_writer(){
    }

    @Override
    public void write(List<Message> toWrite) throws Message_not_exist_exception {
        if(!toWrite.isEmpty()){
            toWrite.forEach(m -> System.out.println(
                    "Tytuł: "
                            + m.getTitle()
                            + "\nAutor: "
                            + m.getAuthor()
                            + "\nContent: "
                            + m.getContent()
                            + "\nDate of creation: "
                            + m.getDateOfCreation()));
        }else
            throw new Message_not_exist_exception("nie ma takiej wiadomości");
    }
}
