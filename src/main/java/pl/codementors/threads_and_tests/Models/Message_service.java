package pl.codementors.threads_and_tests.Models;

import pl.codementors.threads_and_tests.Exceptions.Message_exception;
import pl.codementors.threads_and_tests.Interface.Repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Message_service {
    private Repository repository;
    private List<Message> filtered = new ArrayList<>();

    public Message_service(Repository repository) {
        this.repository = repository;
    }

    public void add_if_no_exists(Message message) throws Message_exception {
        if(repository
                .get_all_messages()
                .contains(message) || message == null){
            throw new Message_exception();
        }else{
            repository.add_message(message);
        }
    }

    public void delete_if_exists(Message message) throws Message_exception {
        if(repository
                .get_all_messages()
                .contains(message)){
            repository.delete_message(message);
        }else{
            throw new Message_exception("Podanej wiadomości nie ma w katalogu");
        }
    }

    public List<Message> get_all_messages(){
        return repository.get_all_messages();
    }

    public List<Message> filter_by_title(String title) throws Message_exception {
        if(title != null){
            filtered = repository
                    .get_all_messages()
                    .stream()
                    .filter(m -> {
                        if(m
                                .getTitle()
                                .equals(title)){
                            return true;
                        }
                        return false;
                    })
                    .collect(Collectors.toList());
        }else{
            throw new Message_exception();
        }
        return Collections.unmodifiableList(filtered);
    }

    public List<Message> filter_by_author(String author) throws Message_exception {
        if(author != null){
            filtered = repository
                    .get_all_messages()
                    .stream()
                    .filter(m -> {
                        if(m
                                .getAuthor()
                                .equalsIgnoreCase(author)){
                            return true;
                        }
                        return false;
                    })
                    .collect(Collectors.toList());
        }else{
            throw new Message_exception();
        }
        return Collections.unmodifiableList(filtered);
    }

    public List<Message> filter_by_content(String content) throws Message_exception {
        if(content != null){
            filtered = repository
                    .get_all_messages()
                    .stream()
                    .filter(m -> {
                        if(m
                                .getContent()
                                .contains(content)){
                            return true;
                        }
                        return false;
                    })
                    .collect(Collectors.toList());
        }else{
            throw new Message_exception();
        }
        return Collections.unmodifiableList(filtered);
    }

    public List<Message> filter_by_date(int day) throws Message_exception {
        if(day > 0 && day <= 31){
            filtered = repository
                    .get_all_messages()
                    .stream()
                    .filter(m -> {
                        if(m
                                .getDateOfCreation()
                                .getDayOfMonth() == day){
                            return true;
                        }
                        return false;
                    })
                    .collect(Collectors.toList());
        }else{
            throw new Message_exception();
        }
        return Collections.unmodifiableList(filtered);
    }

    //FACADE
    public List<Message> multifiltered(String title, String author, String content, int day) throws Message_exception {
        List<Message> messages = new ArrayList<>();

        if(title!=null){
            messages.addAll(filter_by_title(title));
        }
        if(author!=null){
            messages.addAll(filter_by_author(author));
        }
        if(content!=null){
            messages.addAll(filter_by_content(content));
        }
        if(day!=0) {
            messages.addAll(filter_by_date(day));
        }
        return messages;
    }
}
