package pl.codementors.threads_and_tests.Exceptions;

public class Message_exception extends Exception {
    public Message_exception() {
    }

    public Message_exception(String message) {
        super(message);
    }

    public Message_exception(String message, Throwable cause) {
        super(message, cause);
    }

    public Message_exception(Throwable cause) {
        super(cause);
    }

    public Message_exception(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
