package pl.codementors.threads_and_tests;

import pl.codementors.threads_and_tests.Models.Full_writer;
import pl.codementors.threads_and_tests.Models.Message;
import pl.codementors.threads_and_tests.Exceptions.Message_exception;
import pl.codementors.threads_and_tests.Models.Message_catalogue;
import pl.codementors.threads_and_tests.Models.Message_repository;
import pl.codementors.threads_and_tests.Models.Message_service;
import pl.codementors.threads_and_tests.Models.Simple_writer;
import pl.codementors.threads_and_tests.Models.User_search;

import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        boolean is_running = true;
        List<Message> messages;
        Scanner scanner = new Scanner(System.in);
        Message_catalogue message_catalogue = new Message_catalogue();
        Message_repository message_repository = new Message_repository(message_catalogue);
        Message_service message_service = new Message_service(message_repository);
        Simple_writer simple_writer = new Simple_writer();
        Full_writer full_writer = new Full_writer();
        User_search user_search = new User_search();

        System.out.println("WIADOMOŚCI");

        while (is_running){
            System.out.println();
            System.out.println(
                    "MENU"
                    + "\n1 - dodaj wiadomość"
                    + "\n2 - usuń wiadomość"
                    + "\n3 - wyszukaj wiadomość"
                    + "\n4 - zakończ program");

            String user_input = scanner.next();
            switch (user_input){
                case "1":{
                    System.out.println("DODAWANIE WIADOMOŚCI");
                    System.out.println("Podaj tytuł");
                    String title = scanner.next();
                    System.out.println("Podaj autora");
                    String author = scanner.next();
                    System.out.println("Podaj treść wiadomości");
                    String content = scanner.next();

                    Message message = new Message(title, author, content);
                    try {
                        message_service.add_if_no_exists(message);
                        System.out.println("Wiadomość dodana do katalogu");
                        System.out.println("Liczba wiadomości w katalogu: "
                                                    + message_service
                                                    .get_all_messages()
                                                    .size());
                    } catch (Message_exception message_exception) {
                        message_exception.printStackTrace();
                    }
                    break;
                }
                case "2":{
                    System.out.println("USUWANIE WIADOMOŚCI");
                    System.out.println();

                    try {
                        messages = user_search.search_message(message_service, simple_writer, full_writer, scanner);

                        System.out.println();
                        System.out.println("Czy usunąć tą wiadomość\nT - tak\nN - nie");

                        user_input = scanner.next();
                        if(user_input.equalsIgnoreCase("T")){
                                for (Message message_to_delete : messages) {
                                    message_service.delete_if_exists(message_to_delete);
                                }

                                System.out.println(
                                        "Wiadomość usunięta"
                                        +"\nwiadomości w katalogu: "
                                        + message_service
                                                .get_all_messages()
                                                .size());
                        }
                    } catch (Message_exception message_exception) {
                        message_exception.printStackTrace();
                    }
                    break;
                }
                case "3":{
                    try {
                        user_search.search_message(message_service, simple_writer, full_writer, scanner);
                    } catch (Message_exception message_exception) {
                        message_exception.printStackTrace();
                    }
                    break;
                }
                case "4":{
                    System.out.println("BYE!");
                    is_running = false;
                    break;
                }
            }
        }
    }
}